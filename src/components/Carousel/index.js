/* eslint-disable react/no-array-index-key */
/* eslint-disable react/no-unstable-nested-components */
import React, {useState} from 'react'

import PropTypes from 'prop-types'
import Slider from 'react-slick'
import styles from './Carousel.module.scss'
import 'slick-carousel/slick/slick-theme.css'
import 'slick-carousel/slick/slick.css'

import {defaultSettings} from './helpers'

const Carousel = ({
  arrows,
  autoplay,
  center,
  children,
  className,
  dots,
  duration,
  id,
  responsive,
  slidesToShow,
  vertical,
}) => {
  const [currentSlide, setCurrentSlide] = useState(0)
  const slideCount = React.Children.count(children)

  // react-slick settings
  const settings = {
    ...defaultSettings,
    arrows,
    dots,
    slidesToShow,
    autoplay,
    centerMode: center,
    className: `${styles.container} ${className ?? ''}`,
    beforeChange: (_, next) => setCurrentSlide(next),
    vertical,
    verticalSwiping: vertical,
    ...(vertical && {
      appendDots: (slides) => {
        return (
          <ul className={styles.dots} style={{top: '25%', right: '-95%'}}>
            {slides.map((item, index) => {
              return (
                <li
                  key={index}
                  className={styles.nav}
                  style={{display: 'list-item', paddingBottom: '2%'}}
                >
                  {item.props.children}
                </li>
              )
            })}
          </ul>
        )
      },
    }),
    ...(vertical && {
      customPaging: (index) => {
        return (
          <button className={index === currentSlide ? styles.current : null}>{index + 1}</button>
        )
      },
    }),
    ...(autoplay && {autoplaySpeed: duration}),
    ...(responsive && {
      responsive: [
        {
          breakpoint: 2000,
          settings: {
            slidesToShow: 4,
          },
        },
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 1,
            initialSlide: 2,
          },
        },
      ],
    }),
  }

  return (
    <div id={id} className={`${styles.container} ${className ?? ''}`}>
      <div className={styles.counter}>{`${currentSlide + 1}/${slideCount}`}</div>
      <Slider id={id} {...settings}>
        {children?.map((child, index) => (
          <div className={styles.slide} key={index}>
            {child}
          </div>
        ))}
      </Slider>
    </div>
  )
}

Carousel.propTypes = {
  arrows: PropTypes.bool,
  autoplay: PropTypes.bool,
  center: PropTypes.bool,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  dots: PropTypes.bool,
  duration: PropTypes.number,
  id: PropTypes.string,
  responsive: PropTypes.bool,
  slidesToShow: PropTypes.number,
  vertical: PropTypes.bool,
}

Carousel.defaultProps = {
  arrows: false,
  autoplay: false,
  center: false,
  className: null,
  dots: false,
  duration: 3000,
  id: null,
  responsive: false,
  slidesToShow: 1,
  vertical: false,
}

export default Carousel
