// react-slick settings
const defaultSettings = {
  accessibility: true,
  infinite: true,
  slidesToScroll: 1,
  lazyLoad: true,
  adaptiveHeight: true,
  focusOnSelect: true,
  pauseOnHover: true,
}

// eslint-disable-next-line import/prefer-default-export
export {defaultSettings}
