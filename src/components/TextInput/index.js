import React from 'react'

import PropTypes from 'prop-types'
import styles from './TextInput.module.scss'

const TextInput = ({
  ariaLabel,
  className,
  defaultValue,
  disabled,
  error,
  id,
  label,
  name,
  onChange,
  placeholder,
  required,
  type,
  ...props
}) => {
  return (
    <div className={`${styles.container} ${className ?? ''}`}>
      {label && (
        <label className={styles.label} htmlFor={id}>
          {label}
        </label>
      )}
      <input
        aria-describedby={error ? `${id}-error` : null}
        aria-disabled={disabled}
        aria-invalid={Boolean(error)}
        aria-label={ariaLabel}
        aria-placeholder={placeholder}
        className={`${styles.input} ${error ? styles.error : ''}`}
        defaultValue={defaultValue}
        disabled={disabled}
        id={id}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        required={required}
        type={type ?? 'text'}
        {...props}
      />
      {error && (
        <span id={`${id}-error`} className={styles.errorMessage}>
          {error}
        </span>
      )}
    </div>
  )
}

TextInput.propTypes = {
  ariaLabel: PropTypes.string.isRequired,
  className: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.string,
}

TextInput.defaultProps = {
  className: null,
  defaultValue: '',
  disabled: false,
  error: null,
  label: null,
  name: null,
  onChange: null,
  placeholder: null,
  required: false,
  type: 'text',
}

export default TextInput
