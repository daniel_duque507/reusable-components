import React from 'react'

import PropTypes from 'prop-types'
import styles from './TextArea.module.scss'

const TextArea = ({
  ariaLabel,
  className,
  defaultValue,
  disabled,
  error,
  id,
  label,
  maxLength,
  name,
  onChange,
  placeholder,
  required,
  rows,
  ...props
}) => {
  return (
    <div className={`${styles.container} ${className ?? ''}`}>
      {label && (
        <label className={styles.label} htmlFor={id}>
          {label}
        </label>
      )}
      <textarea
        aria-describedby={error ? `${id}-error` : null}
        aria-disabled={disabled}
        aria-invalid={Boolean(error)}
        aria-label={ariaLabel}
        aria-placeholder={placeholder}
        className={`${styles.input} ${error ? styles.inputError : ''}`}
        defaultValue={defaultValue}
        disabled={disabled}
        id={id}
        maxLength={maxLength}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        required={required}
        rows={rows}
        {...props}
      />
      {error && (
        <span id={`${id}-error`} className={styles.errorMessage}>
          {error}
        </span>
      )}
    </div>
  )
}

TextArea.propTypes = {
  ariaLabel: PropTypes.string.isRequired,
  className: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  maxLength: PropTypes.number,
  name: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  rows: PropTypes.number,
}

TextArea.defaultProps = {
  className: null,
  defaultValue: '',
  disabled: false,
  error: null,
  label: null,
  maxLength: null,
  name: null,
  onChange: null,
  placeholder: null,
  required: false,
  rows: null,
}

export default TextArea
