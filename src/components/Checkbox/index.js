import React from 'react'

import PropTypes from 'prop-types'
import styles from './Checkbox.module.scss'

const Checkbox = ({
  ariaDescribedby,
  ariaInvalid,
  ariaLabel,
  checked,
  className,
  disabled,
  id,
  label,
  name,
  onChange,
  readOnly,
  required,
  value,
  ...props
}) => {
  return (
    <div className={`${styles.container} ${className ?? ''}`}>
      <input
        aria-describedby={ariaDescribedby}
        aria-disabled={disabled}
        aria-invalid={ariaInvalid}
        aria-label={ariaLabel}
        checked={checked}
        disabled={disabled}
        id={id}
        name={name}
        onChange={onChange}
        readOnly={readOnly}
        required={required}
        type="checkbox"
        value={value}
        {...props}
      />
      {label && (
        <label className={styles.label} htmlFor={id}>
          {label}
        </label>
      )}
    </div>
  )
}

Checkbox.propTypes = {
  ariaDescribedby: PropTypes.string,
  ariaInvalid: PropTypes.bool,
  ariaLabel: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  value: PropTypes.any,
}

Checkbox.defaultProps = {
  ariaDescribedby: null,
  ariaInvalid: false,
  checked: false,
  className: null,
  disabled: false,
  label: null,
  name: null,
  onChange: null,
  readOnly: false,
  required: false,
  value: null,
}

export default Checkbox
