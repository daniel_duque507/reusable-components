import React from 'react'

import PropTypes from 'prop-types'
// import styles from './Text.module.scss'

import DOMPurify from 'dompurify'
import {sanitizeConfig} from './helpers'

const Text = ({id, className, html, inline, children, ...props}) => {
  // Get semantic tag from variant name, defaults to <span /> if variant is invalid
  const Component = inline ? 'span' : 'p'

  // Sanitize html input
  const sanitizedContent = (dirty) => DOMPurify.sanitize(dirty, sanitizeConfig)

  return (
    <Component
      className={className}
      // eslint-disable-next-line react/no-danger
      // If html is provided, use it, otherwise use children
      dangerouslySetInnerHTML={html ? {__html: sanitizedContent(html)} : null}
      id={id}
      {...props}
    >
      {!html && (children ?? null)}
    </Component>
  )
}

Text.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  html: PropTypes.string,
  id: PropTypes.string,
  inline: PropTypes.bool,
}

Text.defaultProps = {
  children: null,
  className: null,
  html: null,
  id: null,
  inline: false,
}

export default Text
