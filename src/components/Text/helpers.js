// sanitize-html configuration
const sanitizeConfig = {
  ALLOWED_TAGS: [
    'b',
    'q',
    'i',
    'em',
    'strong',
    'a',
    'p',
    'br',
    'span',
    'div',
    'ul',
    'li',
    'ol',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
  ],
  ALLOWED_ATTR: ['style'],
}

// eslint-disable-next-line import/prefer-default-export
export {sanitizeConfig}
