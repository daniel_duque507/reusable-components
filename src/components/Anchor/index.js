import React from 'react'

import PropTypes from 'prop-types'
import styles from './Anchor.module.scss'

const Anchor = ({
  ariaLabel,
  button,
  className,
  icon,
  id,
  label,
  outlined,
  solid,
  target,
  to,
  ...props
}) => {
  return (
    <a
      id={id}
      href={to}
      aria-label={ariaLabel}
      target={target}
      rel={target === '_blank' ? 'noopener noreferrer' : null}
      className={`${styles.anchor} ${button ? styles.button : ''} ${
        button && solid ? styles.solid : ''
      } ${button && outlined ? styles.outlined : ''} ${className ?? ''}`}
      {...props}
    >
      {label}
      {/* Display optional icon */}
      {icon ?? null}
    </a>
  )
}

Anchor.propTypes = {
  ariaLabel: PropTypes.string.isRequired,
  button: PropTypes.bool,
  className: PropTypes.string,
  icon: PropTypes.object,
  id: PropTypes.string,
  label: PropTypes.string,
  outlined: PropTypes.bool,
  solid: PropTypes.bool,
  target: PropTypes.string,
  to: PropTypes.string.isRequired,
}

Anchor.defaultProps = {
  button: false,
  className: null,
  icon: null,
  id: null,
  label: null,
  outlined: false,
  solid: false,
  target: '_self',
}

export default Anchor
