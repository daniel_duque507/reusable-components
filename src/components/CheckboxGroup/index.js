import React, {useEffect, useState} from 'react'

import PropTypes from 'prop-types'
import styles from './CheckboxGroup.module.scss'

import Checkbox from '../Checkbox'

const CheckboxGroup = ({
  className,
  clearErrors,
  disabled,
  error,
  id,
  label,
  name,
  onChange,
  options,
  readOnly,
  required,
  setValue,
  value,
  ...props
}) => {
  const [selected, setSelected] = useState([])

  // Update selection in react-hook-forms
  useEffect(() => {
    setValue(name, selected)
    clearErrors(name)
  }, [setValue, name, selected, clearErrors])

  // Handle checkbox selection
  const handleSelect = (selection) => {
    if (selected.includes(selection)) {
      const filteredData = selected.filter((item) => item !== selection)
      setSelected(filteredData)
    } else {
      setSelected((prevState) => [...prevState, selection])
    }
  }

  return (
    <fieldset id={id} className={`${styles.container} ${className ?? ''}`} {...props}>
      {label && <legend className={styles.label}>{label}</legend>}
      {options?.map((option) => (
        <Checkbox
          aria-describedby={error ? `${id}-error` : null}
          aria-invalid={Boolean(error)}
          ariaLabel={option?.label}
          checked={selected?.includes(option?.value)}
          disabled={disabled}
          id={`${id}-${option?.id}`}
          key={option?.id}
          label={option?.label}
          name={name}
          onChange={() => handleSelect(option?.value)}
          readOnly={readOnly}
          required={required}
          value={option?.value}
        />
      ))}
      {error && (
        <span id={`${id}-error`} className={styles.error}>
          {error}
        </span>
      )}
    </fieldset>
  )
}
CheckboxGroup.propTypes = {
  className: PropTypes.string,
  clearErrors: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.any.isRequired,
      label: PropTypes.string.isRequired,
      value: PropTypes.any.isRequired,
    }),
  ).isRequired,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  setValue: PropTypes.func.isRequired,
  value: PropTypes.any,
}

CheckboxGroup.defaultProps = {
  className: null,
  disabled: false,
  error: null,
  label: null,
  name: null,
  onChange: null,
  readOnly: false,
  required: false,
  value: null,
}

export default CheckboxGroup
