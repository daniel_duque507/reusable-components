import React, {useCallback} from 'react'

import PropTypes from 'prop-types'
import styles from './Button.module.scss'

/**
 * Reusable button component
 */
const Button = ({
  id,
  icon,
  label,
  type,
  disabled,
  ariaLabel,
  onClick,
  solid,
  outlined,
  className,
  ...props
}) => {
  // Handle button click
  const handleClick = useCallback(() => {
    onClick()
  }, [onClick])

  return (
    <button
      aria-disabled={disabled}
      aria-label={ariaLabel}
      className={`${styles.button} ${solid ? styles.solid : ''} ${
        outlined ? styles.outlined : ''
      } ${className ?? ''}`}
      disabled={disabled}
      id={id}
      onClick={onClick ? handleClick : null}
      type={type}
      {...props}
    >
      <div className={styles.content}>
        {label && <span className={styles.label}>{label}</span>}
        {/* Display optional icon */}
        {icon ?? null}
      </div>
    </button>
  )
}

Button.propTypes = {
  id: PropTypes.string,
  icon: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  ariaLabel: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  solid: PropTypes.bool,
  outlined: PropTypes.bool,
  className: PropTypes.string,
}

Button.defaultProps = {
  id: null,
  icon: null,
  label: null,
  type: 'submit',
  disabled: false,
  solid: false,
  outlined: false,
  onClick: null,
  className: null,
}

export default Button
