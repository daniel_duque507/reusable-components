import React, {useEffect, useState} from 'react'

import PropTypes from 'prop-types'
import styles from './RadioButtonGroup.module.scss'

import RadioButton from '../RadioButton'

const RadioButtonGroup = ({
  className,
  clearErrors,
  disabled,
  error,
  id,
  label,
  name,
  onChange,
  options,
  readOnly,
  required,
  setValue,
  value,
  ...props
}) => {
  const [selected, setSelected] = useState()

  // Update selection in react-hook-forms
  useEffect(() => {
    setValue(name, selected)
    clearErrors(name)
  }, [setValue, name, selected, clearErrors])

  // Handle RadioButton selection
  const handleSelect = (selection) => {
    setSelected(selection)
  }

  return (
    <fieldset id={id} className={`${styles.container} ${className ?? ''}`} {...props}>
      {label && <legend className={styles.label}>{label}</legend>}
      {options?.map((option) => (
        <RadioButton
          ariaLabel={option?.label}
          checked={option?.value === selected}
          disabled={disabled}
          id={`${id}-${option?.id}`}
          key={option?.id}
          label={option?.label}
          name={name}
          onChange={() => handleSelect(option?.value)}
          readOnly={readOnly}
          required={required}
          value={option?.value}
        />
      ))}
      {error && (
        <span id={`${id}-error`} className={styles.error}>
          {error}
        </span>
      )}
    </fieldset>
  )
}
RadioButtonGroup.propTypes = {
  className: PropTypes.string,
  clearErrors: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.any.isRequired,
      label: PropTypes.string.isRequired,
      value: PropTypes.any.isRequired,
    }),
  ).isRequired,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  setValue: PropTypes.func.isRequired,
  value: PropTypes.any,
}

RadioButtonGroup.defaultProps = {
  className: null,
  disabled: false,
  error: null,
  label: null,
  name: null,
  onChange: null,
  readOnly: false,
  required: false,
  value: null,
}

export default RadioButtonGroup
