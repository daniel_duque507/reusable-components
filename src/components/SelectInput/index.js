import React from 'react'

import PropTypes from 'prop-types'
import styles from './SelectInput.module.scss'

const SelectInput = ({
  ariaLabel,
  className,
  defaultValue,
  disabled,
  error,
  horizontal,
  id,
  label,
  name,
  onChange,
  options,
  placeholder,
  required,
  ...props
}) => {
  return (
    <div
      className={`${styles.container} ${horizontal ? styles.horizontal : ''} ${
        error ? styles.error : ''
      } ${className ?? ''}`}
    >
      {label && (
        <label className={styles.label} htmlFor={id}>
          {label}
        </label>
      )}
      <select
        aria-describedby={error ? `${id}-error` : null}
        aria-disabled={disabled}
        aria-invalid={Boolean(error)}
        aria-label={ariaLabel}
        defaultValue={defaultValue}
        disabled={disabled}
        id={id}
        name={name}
        onChange={onChange}
        options={options}
        required={required}
        {...props}
      >
        {placeholder && (
          // Set placeholder option
          <option value="" disabled>
            {placeholder}
          </option>
        )}
        {options?.map((option) => (
          <option key={option?.id} value={option?.value}>
            {option?.label}
          </option>
        ))}
      </select>
      {error && (
        <span id={`${id}-error`} className={styles.errorMessage}>
          {error}
        </span>
      )}
    </div>
  )
}

SelectInput.propTypes = {
  ariaLabel: PropTypes.string.isRequired,
  className: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  horizontal: PropTypes.bool,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.any.isRequired,
      label: PropTypes.string.isRequired,
      value: PropTypes.any.isRequired,
    }),
  ).isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
}

SelectInput.defaultProps = {
  className: null,
  defaultValue: '',
  disabled: false,
  error: null,
  horizontal: false,
  label: null,
  name: null,
  onChange: null,
  placeholder: null,
  required: false,
}

export default SelectInput
