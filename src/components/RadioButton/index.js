import React from 'react'

import PropTypes from 'prop-types'
import styles from './RadioButton.module.scss'

const RadioButton = ({
  ariaLabel,
  checked,
  className,
  disabled,
  id,
  label,
  name,
  onChange,
  readOnly,
  required,
  value,
  ...props
}) => {
  return (
    <div className={`${styles.container} ${className ?? ''}`}>
      <input
        aria-disabled={disabled}
        aria-label={ariaLabel}
        checked={checked}
        disabled={disabled}
        id={id}
        name={name}
        onChange={onChange}
        readOnly={readOnly}
        required={required}
        type="radio"
        value={value}
        {...props}
      />
      {label && (
        <label className={styles.label} htmlFor={id}>
          {label}
        </label>
      )}
    </div>
  )
}

RadioButton.propTypes = {
  ariaLabel: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  value: PropTypes.any,
}

RadioButton.defaultProps = {
  checked: false,
  className: null,
  disabled: false,
  label: null,
  name: null,
  onChange: null,
  readOnly: false,
  required: false,
  value: null,
}

export default RadioButton
