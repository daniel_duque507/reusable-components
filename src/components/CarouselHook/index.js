/* eslint-disable no-unused-vars */
import React, {useState, useEffect, useReducer} from 'react'
import PropTypes from 'prop-types'
import {useSwipeable} from 'react-swipeable'
import styles from './Carousel.module.scss'

// This should all be moved to a context provider

const transitionTime = 400
const elastic = `transform ${transitionTime}ms cubic-bezier(0.68, -0.55, 0.265, 1.55)`
const smooth = `transform ${transitionTime}ms ease`

// Initial carousel state
const initialState = {
  offset: 0,
  target: 0,
  active: 0,
}

// Go to next slide
const next = (slideCount, currentSlide) => {
  return (currentSlide + 1) % slideCount
}

// Go to previous slide
const prev = (slideCount, currentSlide) => {
  return (currentSlide - 1 + slideCount) % slideCount
}

const threshold = (target) => {
  const width = target.clientWidth
  return width / 3
}

// Slide reducer
const reducer = (state, action) => {
  switch (action.type) {
    case 'next':
      return {
        ...state,
        target: next(action.length, state.active),
      }
    case 'prev':
      return {
        ...state,
        target: prev(action.length, state.active),
      }
    case 'goto':
      return {
        ...state,
        target: action.target,
      }
    case 'done':
      return {
        ...state,
        offset: null,
        active: state.target,
      }
    case 'drag':
      return {
        ...state,
        offset: action.offset,
      }
    default:
      return state
  }
}

// Handle swiping
const swiped = (e, dispatch, slideCount, direction) => {
  const t = threshold(e.event.target)
  const d = direction * e.deltaX

  if (d >= t) {
    dispatch({
      type: direction > 0 ? 'next' : 'prev',
      slideCount,
    })
  } else {
    dispatch({
      type: 'drag',
      offset: 0,
    })
  }
}

const Carousel = ({id, className, duration, children}) => {
  // eslint-disable-next-line no-unused-vars
  const [state, dispatch] = useReducer(reducer, initialState)
  const [currentSlide, setCurrentSlide] = useState(0)
  const slideCount = React.Children.count(children)
  const [style, setStyle] = useState({
    transform: 'translateX(0)',
    width: `${100 * (slideCount + 2)}%`,
    left: `-${(state.active + 1) * 100}%`,
  })

  // Drag handler
  const handlers = useSwipeable({
    onSwiping(e) {
      dispatch({
        type: 'drag',
        offset: -e.deltaX,
      })
    },
    onSwipedLeft(e) {
      swiped(e, dispatch, slideCount, 1)
    },
    onSwipedRight(e) {
      swiped(e, dispatch, slideCount, -1)
    },
    trackMouse: true,
    trackTouch: true,
  })

  useEffect(() => {
    const timeoutId = setTimeout(() => dispatch({type: 'next', slideCount}), duration)
    return () => clearTimeout(timeoutId)
  }, [state.offset, state.active, duration, slideCount])

  useEffect(() => {
    const timeoutId = setTimeout(() => dispatch({type: 'done'}), transitionTime)
    return () => clearTimeout(timeoutId)
  }, [state.target])

  useEffect(() => {
    if (state.desired !== state.active) {
      const dist = Math.abs(state.active - state.desired)
      const pref = Math.sign(state.offset || 0)
      const dir = (dist > slideCount / 2 ? 1 : -1) * Math.sign(state.desired - state.active)
      const shift = (100 * (pref || dir)) / (slideCount + 2)
      setStyle((prevState) => ({
        ...prevState,
        transition: smooth,
        transform: `translateX(${shift}%)`,
      }))
    } else if (!Number.isNaN(state.offset)) {
      if (state.offset !== 0) {
        setStyle((prevState) => ({
          ...prevState,
          transform: `translateX(${state.offset}px)`,
        }))
      } else {
        setStyle((prevState) => ({
          ...prevState,
          transition: elastic,
        }))
      }
    }
  }, [slideCount, state.active, state.desired, state.offset])

  console.log({state})
  return (
    <div id={id} className={`${styles.container} ${className ?? ''}`}>
      <ul className={styles.controls}>
        {children.map((_, index) => (
          <button
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            aria-label={`Go to slide ${index + 1}`}
            onClick={() => setCurrentSlide(index)}
            className={`${styles.control} ${currentSlide === index ? styles.active : ''}`}
          />
        ))}
      </ul>
      <div className={styles.content} {...handlers} style={style}>
        <div className={styles.slide}>{children[children.length - 1]}</div>
        {children?.map((child, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <div className={styles.slide} key={index}>
            {child}
          </div>
        ))}
        <div className={styles.slide}>{children[0]}</div>
      </div>
    </div>
  )
}

Carousel.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  duration: PropTypes.number,
}

Carousel.defaultProps = {
  id: null,
  className: null,
  duration: 3000,
}

export default Carousel
