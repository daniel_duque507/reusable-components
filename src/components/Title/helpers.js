// Get semantic tag from variant name
const variantMap = {
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
  h5: 'h5',
  h6: 'h6',
}

// sanitize-html configuration
const sanitizeConfig = {
  ALLOWED_TAGS: [
    'b',
    'q',
    'i',
    'em',
    'strong',
    'a',
    'p',
    'br',
    'span',
    'div',
    'ul',
    'li',
    'ol',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
  ],
  ALLOWED_ATTR: ['style'],
}

export {variantMap, sanitizeConfig}
