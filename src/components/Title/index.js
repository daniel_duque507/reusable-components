import React from 'react'

import PropTypes from 'prop-types'
// import styles from './Title.module.scss'

import DOMPurify from 'dompurify'
import {sanitizeConfig, variantMap} from './helpers'

const Title = ({id, variant, className, html, children, ...props}) => {
  // Get semantic tag from variant name, defaults to <span /> if variant is invalid
  const Component = variantMap[variant] ?? 'span'

  // Sanitize html input
  const sanitizedContent = (dirty) => DOMPurify.sanitize(dirty, sanitizeConfig)

  return (
    <Component
      aria-level={Component === 'span' ? '1' : null}
      className={className}
      // If html is provided, use it, otherwise use children
      dangerouslySetInnerHTML={html ? {__html: sanitizedContent(html)} : null}
      id={id}
      role={Component === 'span' ? 'heading' : null}
      {...props}
    >
      {!html && (children ?? null)}
    </Component>
  )
}

Title.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  html: PropTypes.string,
  id: PropTypes.string,
  variant: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']).isRequired,
}

Title.defaultProps = {
  children: null,
  className: null,
  html: null,
  id: null,
}

export default Title
