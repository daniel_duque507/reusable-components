import React from 'react'

import styles from './Buttons.module.scss'

import Button from '../../../components/Button'
import Title from '../../../components/Title'
import Text from '../../../components/Text'

import {ReactComponent as PlayIcon} from '../../../assets/icons/play.svg'

const Buttons = () => {
  const buttonClick = (message) => {
    // eslint-disable-next-line no-console
    console.log(message)
  }

  return (
    <section>
      <Title variant="h2">Button Components</Title>
      <Text>
        These buttons are being rendered using the same prototype reusable component. It has ARIA
        attributes for improved accesibility and it supports text labels, composite labels (icon +
        text) and icon labels. CSS styling is basic at the moment and it&apos;s done with SASS,
        three different predefined button styles are supported (default, outlined, solid)
      </Text>
      <ul>
        <li>
          <Title variant="h3">Button with Text label</Title>
          <div className={styles.row}>
            <Button
              label="Text Label"
              ariaLabel="button-with-text-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
            />
            <Button
              label="Text Label"
              ariaLabel="outlined-button-with-text-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              outlined
            />
            <Button
              label="Text Label"
              ariaLabel="solid-button-with-text-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              solid
            />
          </div>
          <div className={styles.row}>
            <Button
              label="Text Label"
              ariaLabel="disabled-button-with-text-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              disabled
            />
            <Button
              label="Text Label"
              ariaLabel="disabled-outlined-button-with-text-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              outlined
              disabled
            />
            <Button
              label="Text Label"
              ariaLabel="disabled-solid-button-with-text-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              solid
              disabled
            />
          </div>
        </li>
        <li>
          <Title variant="h3">Button with composite label</Title>
          <div className={styles.row}>
            <Button
              label="Watch video"
              icon={<PlayIcon />}
              ariaLabel="button-with-composite-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
            />
            <Button
              label="Watch video"
              icon={<PlayIcon />}
              ariaLabel="outlined-button-with-composite-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              outlined
            />
            <Button
              label="Watch video"
              icon={<PlayIcon />}
              ariaLabel="solid-button-with-composite-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              solid
            />
          </div>
          <div className={styles.row}>
            <Button
              label="Watch video"
              icon={<PlayIcon />}
              ariaLabel="disabled-button-with-composite-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              disabled
            />
            <Button
              label="Watch video"
              icon={<PlayIcon />}
              ariaLabel="disabled-outlined-button-with-composite-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              outlined
              disabled
            />
            <Button
              label="Watch video"
              icon={<PlayIcon />}
              ariaLabel="disabled-solid-button-with-composite-label"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              solid
              disabled
            />
          </div>
        </li>
        <li>
          <Title variant="h3">Icon Button</Title>
          <div className={styles.row}>
            <Button
              id="icon-button"
              icon={<PlayIcon />}
              ariaLabel="icon-button"
              type="button"
              onClick={() => buttonClick('Clicked!')}
            />
            <Button
              id="icon-button"
              icon={<PlayIcon />}
              ariaLabel="icon-button-outlined"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              outlined
            />
            <Button
              id="icon-button"
              icon={<PlayIcon />}
              ariaLabel="icon-button-solid"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              solid
            />
          </div>
          <div className={styles.row}>
            <Button
              id="icon-button"
              icon={<PlayIcon />}
              ariaLabel="disabled-icon-button"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              disabled
            />
            <Button
              id="icon-button"
              icon={<PlayIcon />}
              ariaLabel="disabled-icon-button-outlined"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              outlined
              disabled
            />
            <Button
              id="icon-button"
              icon={<PlayIcon />}
              ariaLabel="disabled-icon-button-solid"
              type="button"
              onClick={() => buttonClick('Clicked!')}
              solid
              disabled
            />
          </div>
        </li>
      </ul>
    </section>
  )
}

export default Buttons
