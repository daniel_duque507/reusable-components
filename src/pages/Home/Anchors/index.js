import React from 'react'

import styles from './Anchors.module.scss'

import Anchor from '../../../components/Anchor'
import Title from '../../../components/Title'
import Text from '../../../components/Text'

import {ReactComponent as PlayIcon} from '../../../assets/icons/play.svg'

const Anchors = () => {
  return (
    <section>
      <Title variant="h2">Anchor Components</Title>
      <Text>Reusable link components</Text>
      <ul>
        <li>
          <Title variant="h3">Simple text link (opens in this tab)</Title>
          <Anchor
            id="text-anchor-here"
            to="https://herodigital.com/"
            ariaLabel="internal-link"
            label="Internal link"
          />
        </li>
        <li>
          <Title variant="h3">Simple text link (opens in new tab)</Title>
          <Anchor
            id="text-anchor-here"
            to="https://herodigital.com/"
            ariaLabel="external-link"
            label="External link"
            target="_blank"
          />
        </li>
        <li>
          <Title variant="h3">Button style link</Title>
          <div className={styles.row}>
            <Anchor
              id="text-anchor-here"
              to="https://herodigital.com/"
              ariaLabel="button-style-link"
              label="External link"
              target="_blank"
              button
            />
            <Anchor
              id="text-anchor-here"
              to="https://herodigital.com/"
              ariaLabel="button-style-oulined-link"
              label="External link"
              target="_blank"
              button
              outlined
            />
            <Anchor
              id="text-anchor-here"
              to="https://herodigital.com/"
              ariaLabel="button-style-solid-link"
              label="External link"
              target="_blank"
              button
              solid
            />
          </div>
        </li>
        <li>
          <Title variant="h3">Button style link with composite label</Title>
          <div className={styles.row}>
            <Anchor
              id="text-anchor-here"
              to="https://herodigital.com/"
              ariaLabel="button-style-composite-link"
              label="External link"
              target="_blank"
              icon={<PlayIcon />}
              button
            />
            <Anchor
              id="text-anchor-here"
              to="https://herodigital.com/"
              ariaLabel="button-style-composite-oulined-link"
              label="External link"
              target="_blank"
              icon={<PlayIcon />}
              button
              outlined
            />
            <Anchor
              id="text-anchor-here"
              to="https://herodigital.com/"
              ariaLabel="button-style-composite-solid-link"
              label="External link"
              target="_blank"
              icon={<PlayIcon />}
              button
              solid
            />
          </div>
        </li>
      </ul>
    </section>
  )
}

export default Anchors
