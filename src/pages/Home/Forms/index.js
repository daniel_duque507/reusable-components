import React from 'react'

import styles from './Forms.module.scss'

import TextInput from '../../../components/TextInput'
import TextArea from '../../../components/TextArea'
import RadioButton from '../../../components/RadioButton'
import Checkbox from '../../../components/Checkbox'
import SelectInput from '../../../components/SelectInput'
import Title from '../../../components/Title'
import Text from '../../../components/Text'

const Forms = () => {
  // Select input options
  const options = [
    {id: 1, value: 'option1', label: 'Option One'},
    {id: 2, value: 'option2', label: 'Option Two'},
    {id: 3, value: 'option3', label: 'Option Three'},
  ]

  return (
    <section>
      <Title variant="h2">Form Components</Title>
      <Text>Reusable form components</Text>
      <ul>
        <li>
          <Title variant="h3">TextField input</Title>
          <div className="home-row">
            <TextInput
              id="text-input"
              type="text"
              placeholder="Text Field"
              ariaLabel="text-input-no-label"
              style={{maxWidth: '500px'}}
            />
          </div>
        </li>
        <li>
          <Title variant="h3">TextField Input with Label</Title>
          <div className="home-row">
            <TextInput
              id="text-input-with-label"
              type="text"
              placeholder="Text Field Label"
              label="Text Field"
              ariaLabel="text-input-with-label"
              style={{maxWidth: '500px'}}
            />
          </div>
        </li>
        <li>
          <Title variant="h3">TextField Input with Active Error Message</Title>
          <div className="home-row">
            <TextInput
              id="text-input-with-label"
              type="text"
              placeholder="Text Field Label"
              error="* This field is required"
              ariaLabel="text-input-with-error"
              style={{maxWidth: '500px'}}
            />
          </div>
        </li>
        <li>
          <Title variant="h3">Textarea input</Title>
          <div className="home-row">
            <TextArea
              id="textarea-input"
              rows={5}
              placeholder="Textarea Field"
              ariaLabel="textarea-input"
              style={{maxWidth: '500px'}}
            />
          </div>
        </li>
        <li>
          <Title variant="h3">Textarea Input with Label</Title>
          <div className="home-row">
            <TextArea
              id="textarea-input-with-label"
              rows={5}
              placeholder="Textarea Field Label"
              label="Textarea Field"
              ariaLabel="textarea-Input-with-label"
              style={{maxWidth: '500px'}}
            />
          </div>
        </li>
        <li>
          <Title variant="h3">Textarea Input with Active Error Message</Title>
          <div className="home-row">
            <TextArea
              id="textarea-input-with-label"
              rows={5}
              placeholder="Textarea Field Label"
              ariaLabel="textarea-with-active-error-message"
              error="* This field is required"
              style={{maxWidth: '500px'}}
            />
          </div>
        </li>
        <li>
          <Title variant="h3">Radio Buttons</Title>
          <legend id="enabled_radio">Enabled</legend>
          <fieldset aria-labelledby="enabled_radio" className={styles.formGroup}>
            <RadioButton
              id="radio-option-1"
              name="radio_input"
              value="unchecked"
              label="Unchecked"
              ariaLabel="enabled-radio-unchecked"
              readOnly
            />
            <RadioButton
              id="radio-option-2"
              name="radio_input"
              value="checked"
              label="Checked"
              ariaLabel="enabled-radio-checked"
              readOnly
              checked
            />
          </fieldset>
          <legend id="disabled_radio">Disabled</legend>
          <fieldset aria-labelledby="disabled_radio" className={styles.formGroup}>
            <RadioButton
              id="radio-option-3"
              name="radio_input2"
              value="disabled_unchecked"
              label="Disabled Unchecked"
              ariaLabel="disabled-radio-unchecked"
              disabled
            />
            <RadioButton
              id="radio-option-4"
              name="radio_input2"
              value="disabled_checked"
              label="Disabled Checked"
              ariaLabel="disabled-radio-checked"
              disabled
              checked
            />
          </fieldset>
        </li>
        <li>
          <Title variant="h3">Checkboxes</Title>
          <legend id="enabled_radio">Enabled</legend>
          <fieldset aria-labelledby="enabled_radio" className={styles.formGroup}>
            <Checkbox
              id="checkbox-option-1"
              name="checkbox_input"
              value="unchecked"
              label="Unchecked"
              ariaLabel="enabled-checkbox-unchecked"
              readOnly
            />
            <Checkbox
              id="radio-option-2"
              name="radio_input"
              value="checked"
              label="Checked"
              ariaLabel="enabled-checkbox-checked"
              readOnly
              checked
            />
          </fieldset>
          <legend id="disabled_checkbox">Disabled</legend>
          <fieldset aria-labelledby="disabled_checkbox" className={styles.formGroup}>
            <Checkbox
              id="checkbox-option-3"
              name="checkbox_input2"
              value="disabled_unchecked"
              label="Disabled Unchecked"
              ariaLabel="disabled-checkbox-checked"
              disabled
            />
            <Checkbox
              id="checkbox-option-4"
              name="checkbox_input2"
              value="disabled_checked"
              label="Disabled Checked"
              ariaLabel="disabled-checkbox-unchecked"
              disabled
              checked
            />
          </fieldset>
        </li>
        <li>
          <Title variant="h3">Vertical Select</Title>
          <SelectInput
            id="vertical-select"
            name="vertical_select"
            options={options}
            label="Select label"
            ariaLabel="vertical-select"
            style={{maxWidth: '500px'}}
          />
        </li>
        <li>
          <Title variant="h3">Horizontal Select</Title>
          <SelectInput
            id="horizontal-select"
            name="horizontal_select"
            options={options}
            label="Select label"
            ariaLabel="horizontal-select"
            horizontal
            style={{maxWidth: '500px'}}
          />
        </li>
        <li>
          <Title variant="h3">Vertical Select with error</Title>
          <SelectInput
            id="vertical-select-error"
            name="vertical_Select_error"
            options={options}
            label="Select label"
            ariaLabel="invalid-select"
            error="* Invalid choice"
            style={{maxWidth: '500px'}}
          />
        </li>
      </ul>
    </section>
  )
}

export default Forms
