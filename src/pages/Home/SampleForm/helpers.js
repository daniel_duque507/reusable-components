const getErrors = (errors) => {
  const errorList = []
  Object.keys(errors).map(
    (key) =>
      errors[key] &&
      errorList.push({
        id: key,
        message: errors[key]?.message,
      }),
  )
  return errorList
}

// eslint-disable-next-line import/prefer-default-export
export {getErrors}
