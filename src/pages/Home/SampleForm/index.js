import React from 'react'

import {Controller, useForm, useFormState} from 'react-hook-form'
import styles from './SampleForm.module.scss'

import TextInput from '../../../components/TextInput'
import TextArea from '../../../components/TextArea'
import Button from '../../../components/Button'
import CheckboxGroup from '../../../components/CheckboxGroup'
import RadioButtonGroup from '../../../components/RadioButtonGroup'
import SelectInput from '../../../components/SelectInput'
import Anchor from '../../../components/Anchor'
import Title from '../../../components/Title'
import Text from '../../../components/Text'

import * as validate from './validators'
import {getErrors} from './helpers'

const SampleForm = () => {
  // Load react-hook-form
  const {handleSubmit, control, setValue, clearErrors, watch} = useForm()
  const {errors: errorsArray} = useFormState({control})

  // Handle form submission
  const onSubmit = (data) => {
    // eslint-disable-next-line no-console
    console.log('Form contents: ', data)
  }

  // Options array for checkbox input
  const productOptions = [
    {id: 0, label: 'News', value: 'news'},
    {id: 1, label: 'Offers', value: 'offers'},
    {id: 2, label: 'Private Messages', value: 'pm'},
  ]

  // Options array for radio input
  const agreeOptions = [
    {id: 0, label: 'Yes', value: true},
    {id: 1, label: 'No', value: false},
  ]

  // Options array for select input
  const selectOptions = [
    {id: 0, label: 'Personal use', value: 'personal'},
    {id: 1, label: 'Professional use', value: 'professional'},
  ]

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={styles.sampleform}>
      <Title variant="h2">Sample Form</Title>
      <Text>
        This sample form makes use of the reusable component library and displays the form results
        in the browser&apos;s console. The form is being processed using the{' '}
        <Anchor
          to="https://react-hook-form.com/"
          ariaLabel="react-hook-form"
          label="react-hook-form"
          target="_blank"
        />{' '}
        library.
      </Text>
      {getErrors(errorsArray)?.length > 0 && (
        <div className={styles.formErrors}>
          <p>{`There ${getErrors(errorsArray)?.length > 1 ? 'were' : 'was'} (${
            getErrors(errorsArray)?.length
          }) error${getErrors(errorsArray)?.length > 1 ? 's' : ''} found in the form.`}</p>
          {getErrors(errorsArray)?.map((error) => (
            <Anchor
              key={error?.id}
              to={`#${error?.id}`}
              ariaLabel={error?.message}
              label={error?.message}
              target="_self"
            />
          ))}
        </div>
      )}
      <Controller
        name="name"
        control={control}
        rules={{
          required: 'The "Name" field is required',
          pattern: {
            value: validate?.name,
            message: 'Invalid name format',
          },
        }}
        render={({field: {onChange, value}, formState: {errors}}) => (
          <TextInput
            id="name"
            type="text"
            name="name"
            placeholder="First and last name"
            label="Name"
            ariaLabel="full-name"
            defaultValue={value}
            onChange={onChange}
            error={errors?.name?.message}
            required
            className={styles.item}
          />
        )}
      />
      <Controller
        name="email"
        control={control}
        rules={{
          required: 'The "Email" field is required',
          pattern: {
            value: validate?.email,
            message: 'Invalid email format',
          },
        }}
        render={({field: {onChange, value}, formState: {errors}}) => (
          <TextInput
            id="email"
            type="email"
            name="email"
            placeholder="name@company.com"
            label="Email"
            ariaLabel="email-address"
            defaultValue={value}
            onChange={onChange}
            error={errors?.email?.message}
            required
            className={styles.item}
          />
        )}
      />
      <Controller
        name="phone"
        control={control}
        rules={{
          pattern: {
            value: validate?.phone,
            message: 'The phone number format is invalid',
          },
        }}
        render={({field: {onChange, value}, formState: {errors}}) => (
          <TextInput
            id="phone"
            type="phone"
            name="phone"
            placeholder="+1 (786) 220-1341"
            label="Phone number"
            ariaLabel="phone-number"
            defaultValue={value}
            onChange={onChange}
            error={errors?.phone?.message}
            required
            className={styles.item}
          />
        )}
      />
      <Controller
        name="usage"
        control={control}
        rules={{
          required: 'You must select an option for product use',
        }}
        render={({field: {onChange, value}, formState: {errors}}) => (
          <SelectInput
            id="usage"
            name="usage"
            label="Is this for personal or professional use?"
            ariaLabel="is-this-for-personal-or-professional-use?"
            options={selectOptions}
            defaultValue={value}
            onChange={onChange}
            placeholder="Select an option"
            error={errors?.phone?.message}
            required
            className={styles.item}
          />
        )}
      />
      <Controller
        name="comments"
        control={control}
        render={({field: {onChange, value}, formState: {errors}}) => (
          <TextArea
            id="comments"
            name="comments"
            rows={5}
            label="Comments"
            ariaLabel="Comments"
            defaultValue={value}
            onChange={onChange}
            error={errors?.comments?.message}
            className={styles.item}
          />
        )}
      />
      <Controller
        name="products"
        control={control}
        rules={{
          required: 'You must select at least one product',
        }}
        render={({field: {onChange, value}, formState: {errors}}) => (
          <CheckboxGroup
            id="products"
            label="Select products"
            name="products"
            value={value}
            onChange={onChange}
            options={productOptions}
            error={errors?.products?.message}
            className={styles.item}
            setValue={setValue}
            clearErrors={clearErrors}
          />
        )}
      />
      <Controller
        name="agree"
        control={control}
        rules={{
          required: 'You must agree to the terms and conditions',
          pattern: {
            value: watch('agree') === true,
            message: 'You must agree to the terms and conditions',
          },
        }}
        render={({field: {onChange}, formState: {errors}}) => (
          <RadioButtonGroup
            id="agree"
            label="Do you agree with the terms and conditions?"
            name="agree"
            onChange={onChange}
            options={agreeOptions}
            error={errors?.agree?.message}
            className={styles.item}
            setValue={setValue}
            clearErrors={clearErrors}
          />
        )}
      />
      <Button label="Submit" ariaLabel="submit-form" type="submit" solid />
    </form>
  )
}

export default SampleForm
