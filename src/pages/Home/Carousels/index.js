import React from 'react'

import styles from './Carousels.module.scss'

import Title from '../../../components/Title'
import Text from '../../../components/Text'
import Carousel from '../../../components/Carousel'

const Carousels = () => {
  return (
    <section>
      <Title variant="h2">Carousel Component</Title>
      <Text>
        Reusable carousel component. It is accesible, responsive, and supports various other
        functionalities. The styling of the arrows, slide counter and dot navigators can be
        customized to fit the design team&apos;s specifications.
      </Text>
      <ul>
        <li>
          <Title variant="h3">Multiple mode</Title>
          <Carousel autoplay arrows dots slidesToShow={4} duration={3000} responsive>
            <div className={styles.slide}>ONE</div>
            <div className={styles.slide}>TWO</div>
            <div className={styles.slide}>THREE</div>
            <div className={styles.slide}>FOUR</div>
            <div className={styles.slide}>FIVE</div>
            <div className={styles.slide}>SIX</div>
          </Carousel>
        </li>
        <li>
          <Title variant="h3">Center mode</Title>
          <Carousel
            autoplay
            arrows
            dots
            slidesToShow={1}
            duration={3000}
            className={styles.container}
          >
            <div className={styles.big}>ONE</div>
            <div className={styles.big}>TWO</div>
            <div className={styles.big}>THREE</div>
            <div className={styles.big}>FOUR</div>
            <div className={styles.big}>FIVE</div>
            <div className={styles.big}>SIX</div>
          </Carousel>
        </li>
        <li>
          <Title variant="h3">Vertical mode</Title>
          <Carousel
            autoplay
            duration={3000}
            slidesToShow={1}
            vertical
            dots
            className={styles.container}
          >
            <div className={styles.big}>ONE</div>
            <div className={styles.big}>TWO</div>
            <div className={styles.big}>THREE</div>
            <div className={styles.big}>FOUR</div>
            <div className={styles.big}>FIVE</div>
            <div className={styles.big}>SIX</div>
          </Carousel>
        </li>
      </ul>
    </section>
  )
}

export default Carousels
