import React from 'react'

import styles from './Home.module.scss'

import Buttons from './Buttons'
import Carousels from './Carousels'
import Anchors from './Anchors'
import Forms from './Forms'
import SampleForm from './SampleForm'
import Title from '../../components/Title'

const Home = () => {
  return (
    <main className={styles.main}>
      <Title variant="h1">Preliminary Design for Reusable Components</Title>
      <Carousels />
      <Buttons />
      <Anchors />
      <Forms />
      <SampleForm />
    </main>
  )
}

export default Home
